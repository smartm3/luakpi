local ssap_utils = {}
local xml = require("xml")

function ssap_utils.status(message)

   xmlreply = xml.load(message)
   local sect = xml.find(xmlreply, "parameter", "status")
   if sect then
      if sect[1] == "m3:Success" then
	 return true
      else
	 return false
      end
   end

end

return ssap_utils
