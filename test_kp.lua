-- import the library
local KP = require("luakpi")

-- Test methods
kp = KP:new("localhost", 10010, true)
kp:join()
kp:sparql_update('INSERT DATA { <http://ns#test_subject> <http://ns#test_predicate> "test_object"}')
kp:leave()
