-- Requirements
local uuid = require("uuid")
local socket = require("socket")
local ssap_utils = require("ssap_utils")
local ssap_templates = require("ssap_templates")

-- KP Class
KP = {}

-- INIT method
function KP:new(host, port, verbose)

   -- debug print
   if verbose then
      print("Initializing the KP class")
   end

   -- determine a node_id
   node_id = uuid()

   -- setting the initial transaction id
   transaction_id = 0
   
   -- initialization of the KP class
   newObj = {host = host, 
	     port = port, 
	     verbose = verbose,
	     node_id = node_id,
	     transaction_id = transaction_id
   }  
   self.__index = self

   -- return
   return setmetatable(newObj, self)

end


-- JOIN method
function KP:join()

   -- debug print
   if self.verbose then
      print("JOIN method called")
   end

   -- build the message
   msg = ssap_templates.join(self.node_id, self.transaction_id)
   if self.verbose then
      print("Sending:")
      print(msg)
   end
   
   -- send the message
   local tcp = assert(socket.tcp())
   tcp:connect(self.host, self.port)
   tcp:settimeout(1);
   tcp:send(msg);  

   -- waiting for a reply
   reply = ""
   while true do
      local rmsg, stat = tcp:receive(1)    
      if stat == 'closed' or stat == 'timeout' then
	 break
      end
      if rmsg and stat ~= 'timeout' then
	 reply = reply .. rmsg
      end
   end
   if self.verbose then
      print("Received:")
      print(reply)   
   end

   -- parse the message
   status = ssap_utils.status(reply)
   if self.verbose then
      print(string.format("JOIN confirmed: %s", status))
   end

   -- update the transaction id
   self.transaction_id = self.transaction_id + 1
	 
end

-- LEAVE method
function KP:leave()

   -- debug print
   if self.verbose then
      print("LEAVE method called")
   end

   -- build the message
   msg = ssap_templates.leave(self.node_id, self.transaction_id)
   if self.verbose then
      print("Sending:")
      print(msg)
   end
   
   -- send the message
   local tcp = assert(socket.tcp())
   tcp:connect(self.host, self.port)
   tcp:settimeout(1);
   tcp:send(msg);  

   -- waiting for a reply
   reply = ""
   while true do
      local rmsg, stat = tcp:receive(1)    
      if stat == 'closed' or stat == 'timeout' then
	 break
      end
      if rmsg and stat ~= 'timeout' then
	 reply = reply .. rmsg
      end
   end
   if self.verbose then
      print("Received:")
      print(reply)   
   end

   -- parse the message
   status = ssap_utils.status(reply)
   if self.verbose then
      print(string.format("LEAVE confirmed: %s", status))
   end

   -- update the transaction id
   self.transaction_id = self.transaction_id + 1
	 
end


-- SPARQL UPDATE method
function KP:sparql_update(update)

   -- debug print
   if self.verbose then
      print("SPARQL UPDATE method called")
   end

   -- build the message
   msg = ssap_templates.sparqlupdate(self.node_id, self.transaction_id, update)
   if self.verbose then
      print("Sending:")
      print(msg)
   end
   
   -- send the message
   local tcp = assert(socket.tcp())
   tcp:connect(self.host, self.port)
   tcp:settimeout(1);
   tcp:send(msg);  

   -- waiting for a reply
   reply = ""
   while true do
      local rmsg, stat = tcp:receive(1)    
      if stat == 'closed' or stat == 'timeout' then
	 break
      end
      if rmsg and stat ~= 'timeout' then
	 reply = reply .. rmsg
      end
   end
   if self.verbose then
      print("Received:")
      print(reply)   
   end

   -- parse the message
   status = ssap_utils.status(reply)
   if self.verbose then
      print(string.format("UPDATE confirmed: %s", status))
   end

   -- update the transaction id
   self.transaction_id = self.transaction_id + 1

end


return KP
