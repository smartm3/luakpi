local ssap_templates = {}

function ssap_templates.join(node_id, transaction_id)
   
   local join_msg = [[<SSAP_message>
   <node_id>]] .. node_id .. [[</node_id>
   <space_id>X</space_id>
   <transaction_type>JOIN</transaction_type>
   <message_type>REQUEST</message_type>
   <transaction_id>]] .. transaction_id .. [[</transaction_id>
   <parameter name = "credentials">XYZZY</parameter>
   </SSAP_message>]]
      
   return join_msg

end

function ssap_templates.leave(node_id, transaction_id)
   
   local leave_msg = [[<SSAP_message>
   <node_id>]] .. node_id .. [[</node_id>
   <space_id>X</space_id>
   <transaction_type>LEAVE</transaction_type>
   <message_type>REQUEST</message_type>
   <transaction_id>]] .. transaction_id .. [[</transaction_id>
   <parameter name = "credentials">XYZZY</parameter>
   </SSAP_message>]]
      
   return leave_msg

end

function ssap_templates.sparqlupdate(node_id, transaction_id, update)

   -- sanitize input
   update = string.gsub(update, "<", "&lt;")
   update = string.gsub(update, ">", "&gt;")
   update = string.gsub(update, '"', "&quot;")

   -- build the query
   local update_msg = [[<SSAP_message>
   <transaction_type>QUERY</transaction_type>
   <message_type>REQUEST</message_type>
   <transaction_id>]] .. transaction_id .. [[</transaction_id>
   <node_id>]] .. node_id .. [[</node_id><space_id>X</space_id>
   <parameter name = "type">sparql</parameter>
   <parameter name = "query">]] .. update .. [[</parameter></SSAP_message>]]
     
   return update_msg

end

return ssap_templates
